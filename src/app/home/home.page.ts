import {
  Component
} from '@angular/core';
import {
  ToastController
} from '@ionic/angular';
import {
  ModalController
} from '@ionic/angular';
import {
  TaskManagerPage
} from '../task-manager/task-manager.page'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  today: number = Date.now();

  tasks = [];

  categories : string[] = [];

  constructor(
    public modalController: ModalController,
    public toastController: ToastController
  ) {}


  async editTask(selectedIndex: number) {
    const modal = await this.modalController.create({
      component: TaskManagerPage,
      backdropDismiss: false,
      componentProps: {
        selectedIndex: selectedIndex,
        tasks: this.tasks,
        categories: this.categories
      }
    })

    modal.onDidDismiss().then(data => {
      this.categories = data.data.categories;
      this.tasks = data.data.tasks;
    })

    return await modal.present();
  }

  deleteTask(index: number) {
    this.tasks.splice(index, 1);
  }


}
