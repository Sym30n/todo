import {
  Component,
  OnInit
} from '@angular/core';
import {
  ModalController
} from '@ionic/angular';
import {
  CategoryFormPage
} from '../category-form/category-form.page'

@Component({
  selector: 'app-category-manager',
  templateUrl: './category-manager.page.html',
  styleUrls: ['./category-manager.page.scss'],
})
export class CategoryManagerPage implements OnInit {

  categories : string[];

  constructor(public modalController: ModalController) {}

  ngOnInit() {}

  async editCategory(index: number) {
    const modal = await this.modalController.create({
      component: CategoryFormPage,
      backdropDismiss: false,
      componentProps: {
        category: this.categories[index]
      }
    })

    modal.onDidDismiss().then(async category => {
      if (index !== undefined) {
        if (category.data === undefined) {
          this.categories.splice(index, 1);
        } else {
          this.categories[index] = category.data;
        }
      } else {
        if (category !== undefined) {
          this.categories.push(category.data);
        }
      }

    })

    return await modal.present();
  }

  async cancel() {
    await this.modalController.dismiss(this.categories);
  }
}
