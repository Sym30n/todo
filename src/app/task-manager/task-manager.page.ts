import {
  Component,
  OnInit
} from '@angular/core';
import {
  ModalController,
  ToastController
} from '@ionic/angular';
import {
  CategoryManagerPage
} from '../category-manager/category-manager.page';

@Component({
  selector: 'app-task-manager',
  templateUrl: './task-manager.page.html',
  styleUrls: ['./task-manager.page.scss'],
})
export class TaskManagerPage implements OnInit {

  categories : string[];
  tasks : [{
    name : string;
    dueDate : string;
    category : string;
    priority : string;
  }];
  selectedIndex : number;

  form_name : string;
  form_dueDate : string;
  form_category : string;
  form_priority : string;

  constructor(
    public modalController: ModalController,
    public toastController: ToastController
  ) {}

  ngOnInit() {
    if (this.selectedIndex != undefined) {
      this.form_name = this.tasks[this.selectedIndex].name;
      this.form_dueDate = this.tasks[this.selectedIndex].dueDate;
      this.form_category = this.tasks[this.selectedIndex].category;
      this.form_priority = this.tasks[this.selectedIndex].priority;
    }
  }


  cancel() {
    this.modalController.dismiss({
      categories: this.categories,
      tasks: this.tasks
    });
  }

  toggleCategory(index) {
    if (this.form_category !== this.categories[index]) {
      this.form_category = this.categories[index];
    } else {
      this.form_category = undefined;
    }
  }

  async save() {
    if (this.form_name) {
      const task = {
        name: this.form_name,
        dueDate: this.form_dueDate,
        category: this.form_category,
        priority: this.form_priority
      };

      if (this.selectedIndex !== undefined) {
        this.tasks[this.selectedIndex] = task;
      } else {
        this.tasks.push(task);
      }

      this.cancel();
    } else {
      const toast = await this.toastController.create({
        message: 'A task must have a name!',
        duration: 2000
      });
      toast.present();
    }
  }


  async editCategories() {
    const modal = await this.modalController.create({
      component: CategoryManagerPage,
      backdropDismiss: false,
      componentProps: {
        categories: this.categories
      }
    })

    modal.onDidDismiss().then(categoryArray => {
      if (categoryArray.data !== undefined) {
        this.categories = categoryArray.data;
      }
    })

    return await modal.present();
  }

}
