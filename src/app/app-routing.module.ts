import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'task-manager',
    loadChildren: () => import('./task-manager/task-manager.module').then( m => m.TaskManagerPageModule)
  },
  {
    path: 'category-manager',
    loadChildren: () => import('./category-manager/category-manager.module').then( m => m.CategoryManagerPageModule)
  },
  {
    path: 'category-form',
    loadChildren: () => import('./category-form/category-form.module').then( m => m.CategoryFormPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
