import {
  Component,
  OnInit
} from '@angular/core';
import {
  ModalController, ToastController
} from '@ionic/angular';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.page.html',
  styleUrls: ['./category-form.page.scss'],
})
export class CategoryFormPage implements OnInit {

  category : string;
  categoryOrigin : string;

  constructor(
    public modalController: ModalController,
    public toastController: ToastController) {}

  ngOnInit() {
    if (this.category !== undefined) {
      this.categoryOrigin = this.category;
    }
  }

  async saveCategory() {
    if(this.category){
      await this.modalController.dismiss(this.category);
    } else 
    {
      const toast = await this.toastController.create({
        message: 'A task must have a name!',
        duration: 2000
      });
      toast.present();
    }
  }

  async deleteCategory() {
    await this.modalController.dismiss();
  }

  async cancel() {
    if (this.categoryOrigin !== undefined) {
      await this.modalController.dismiss(this.categoryOrigin);
    } else {
      await this.modalController.dismiss();
    }
  }


}
